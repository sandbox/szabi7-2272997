<?php

/**
 * @file
 * Recent visits views handler file
 */

/**
 * Implements hook_views_data_alter().
 */
function recent_views_data_alter(&$data) {
  $data['recent']['uid']['field']['handler'] = 'RecentViewsHandlerFieldUid';
  $data['recent']['nid']['field']['handler'] = 'RecentViewsHandlerFieldNid';
  $data['recent']['timestamp']['field']['handler'] = 'RecentViewsHandlerFieldTim';
}

/**
 * Implements hook_views_query_substitution().
 */
function recent_views_query_substitutions($view) {
  global $user;
  $shash = '';
  if ($user->uid == 0) {
    if (isset($_COOKIE['Drupal_visitor_recent'])) {
      $shash = $_COOKIE['Drupal_visitor_recent'];
    }
  }
  return array(
    '***CURRENT_UID***' => $user->uid,
    '***CURRENT_HASH***' => $shash,
  );
}
