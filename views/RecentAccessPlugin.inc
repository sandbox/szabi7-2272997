<?php

/**
 * @file
 * Recent visits Views Access plugin
 */

class RecentAccessPlugin extends views_plugin_access {
  /**
   * Impl summary titile().
   */
  public function summary_title() {
    return t('Recent Own Visits access plugin');
  }
  /**
   * Impl access().
   */
  public function access($account) {
    return recent_access($account);
  }
  /**
   * Impl get_access_callback().
   */
  public function get_access_callback() {
    return array('recent_access', array());
  }
}
