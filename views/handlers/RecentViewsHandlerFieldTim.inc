<?php

/**
 * @file
 * Recent visits views handler file
 */

class RecentViewsHandlerFieldTim extends views_handler_field {
  /**
   * Impl render().
   */
  public function render($values) {
    $value = $this->get_value($values);
    return date("Y-m-d H:i:s", $value);
  }
}
