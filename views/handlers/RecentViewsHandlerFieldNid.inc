<?php

/**
 * @file
 * Recent visits views handler file
 */

class RecentViewsHandlerFieldNid extends views_handler_field {
  /**
   * Impl init().
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);
    if (!empty($this->options['link_to_node'])) {
      $this->additional_fields['nid'] = array('table' => 'recent', 'field' => 'nid');
      if (module_exists('translation')) {
        $this->additional_fields['language'] = array('table' => 'node', 'field' => 'language');
      }
    }
  }
  /**
   * Impl option_definition().
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['link_to_node'] = array('default' => isset($this->definition['link_to_node default']) ? $this->definition['link_to_node default'] : FALSE, 'bool' => TRUE);
    return $options;
  }
  /**
   * Impl options_form().
   */
  public function options_form(&$form, &$form_state) {
    $form['link_to_node'] = array(
      '#title' => t('Link this title to the full node.'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_node']),
    );
    parent::options_form($form, $form_state);
  }
  /**
   * Impl render_link().
   */
  public function render_link($data, $values) {
    if (!empty($this->options['link_to_node']) && !empty($this->additional_fields['nid'])) {
      if ($data !== NULL && $data !== '') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = "node/" . $this->get_value($values, 'nid');
        if (isset($this->aliases['language'])) {
          $languages = language_list();
          $language = $this->get_value($values, 'language');
          if (isset($languages[$language])) {
            $this->options['alter']['language'] = $languages[$language];
          }
          else {
            unset($this->options['alter']['language']);
          }
        }
      }
      else {
        $this->options['alter']['make_link'] = FALSE;
      }
    }
    return $data;
  }
  /**
   * Impl render().
   */
  public function render($values) {
    $value = $this->get_value($values);
    $nodel = node_load($value);
    return $this->render_link($this->sanitize_value($nodel->title), $values);
  }
}
