<?php

/**
 * @file
 * Recent visits views handler file
 */

class RecentViewsHandlerFieldUid extends views_handler_field {
  /**
   * Impl init().
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);
    if (!empty($this->options['link_to_user'])) {
      $this->additional_fields['uid'] = array('table' => 'recent', 'field' => 'uid');
      if (module_exists('translation')) {
        $this->additional_fields['language'] = array('table' => 'node', 'field' => 'language');
      }
    }
  }
  /**
   * Impl option_definition().
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['link_to_user'] = array('default' => isset($this->definition['link_to_user default']) ? $this->definition['link_to_user default'] : FALSE, 'bool' => TRUE);
    return $options;
  }
  /**
   * Impl options_form().
   */
  public function options_form(&$form, &$form_state) {
    $form['link_to_user'] = array(
      '#title' => t("Link this username to the user's page."),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_user']),
    );
    parent::options_form($form, $form_state);
  }
  /**
   * Impl render_link().
   */
  public function render_link($data, $values) {
    if (!empty($this->options['link_to_user']) && !empty($this->additional_fields['uid'])) {
      if ($data !== NULL && $data !== '' && ($this->get_value($values, 'uid') != 0)) {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = "user/" . $this->get_value($values, 'uid');
        if (isset($this->aliases['language'])) {
          $languages = language_list();
          $language = $this->get_value($values, 'language');
          if (isset($languages[$language])) {
            $this->options['alter']['language'] = $languages[$language];
          }
          else {
            unset($this->options['alter']['language']);
          }
        }
      }
      else {
        $this->options['alter']['make_link'] = FALSE;
      }
    }
    return $data;
  }
  /**
   * Impl render().
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!$value) {
      $usern = 'anonymous';
    }
    else {
      $userl = user_load($value);
      $usern = $userl->name;
    }
    return $this->render_link($this->sanitize_value($usern), $values);
  }
}
