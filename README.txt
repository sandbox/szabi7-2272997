
-- SUMMARY --

The Recent visits module tracks the visited nodes by different users. It creates 
two blocks: My recent visits, Most visited nodes; and one page where a certain
user's recent visits are shown.

For a full description of the module, visit the project page:
  https://drupal.org/project/2272997

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2272997


-- REQUIREMENTS --

Views , Chaos tools , Entity API , Elysia Cron

-- INSTALLATION --

* Install as usual.

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Which user roles are tracked

  - Which user can see own and others recent visits

* Configure the module settings in Modules >> Recent >> Configure, or use the 
url admin/config/administration/recent:

  - How many days to keep the visits history
